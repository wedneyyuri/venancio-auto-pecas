<?php

require_once("phpmailer/class.phpmailer.php");

$mail             = new PHPMailer();
$mail->CharSet = 'UTF-8';
$mail->IsSMTP(); // telling the class to use SMTP
$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
                                           // 1 = errors and messages
                                           // 2 = messages only
$mail->SMTPAuth   = true;                  // enable SMTP authentication
$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
$mail->Host       = "smtp.zoho.com";      // sets GMAIL as the SMTP server
$mail->Port       = 465;                   // set the SMTP port for the GMAIL server
$mail->Username   = "naoresponda@venancioautopecas.com.br";  // GMAIL username
$mail->Password   = "HERE_WAS_MY_PASSWORD";            // GMAIL password

$mail->SetFrom('naoresponda@venancioautopecas.com.br', 'Não responda');

//$mail->AddReplyTo("name@yourdomain.com","First Last");

$mail->Subject    = "Formulário de Cadastro - test_venancio@gmail.com";

$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

$mail->MsgHTML("Hello World");

$address = "test_venancio@gmail.com";
$mail->AddAddress($address, "Tester");

//$mail->AddAttachment("images/phpmailer.gif");      // attachment
//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment

if(!$mail->Send()) {
  echo "Mailer Error: " . $mail->ErrorInfo;
} else {
  echo "Message sent. Essa mensagem foi enviada para você.!";
}

?>