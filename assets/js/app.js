/**
* Website Module
*
* Main module
*/
angular.module('Website', ['ngRoute'])

.config(function($routeProvider, $locationProvider) {
  $routeProvider
   .when('/', {
    templateUrl: 'app/view/index.html',
    controller: 'IndexController',
  })
   .when('/servicos', {
    templateUrl: 'app/view/servicos.html',
    controller: 'ServicosController',
  })
   .when('/localizacao', {
    templateUrl: 'app/view/localizacao.html',
    controller: 'LocalizacaoController',
  })
  .when('/contato', {
    templateUrl: 'app/view/contato.html',
    controller: 'ContatoController'
  })
  .otherwise('/');

  $locationProvider.html5Mode(true);
})
.run(['$rootScope', function($rootScope){
  angular.extend($rootScope, {
    email: 'venancioautopecas@ig.com.br',
    tel: ['3217-4688', '24*17655']
  });
}])
.config(['$httpProvider',function($httpProvider) {
  // Use x-www-form-urlencoded Content-Type
  $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

  /**
   * The workhorse; converts an object to x-www-form-urlencoded serialization.
   * @param {Object} obj
   * @return {String}
   */ 
  var param = function(obj) {
    var query = '', name, value, fullSubName, subName, subValue, innerObj, i;
      
    for(name in obj) {
      value = obj[name];
        
      if(value instanceof Array) {
        for(i=0; i<value.length; ++i) {
          subValue = value[i];
          fullSubName = name + '[' + i + ']';
          innerObj = {};
          innerObj[fullSubName] = subValue;
          query += param(innerObj) + '&';
        }
      }
      else if(value instanceof Object) {
        for(subName in value) {
          subValue = value[subName];
          fullSubName = name + '[' + subName + ']';
          innerObj = {};
          innerObj[fullSubName] = subValue;
          query += param(innerObj) + '&';
        }
      }
      else if(value !== undefined && value !== null)
        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
    }
      
    return query.length ? query.substr(0, query.length - 1) : query;
  };

  // Override $http service's default transformRequest
  $httpProvider.defaults.transformRequest = [function(data) {
    return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
  }];
}])
.controller('IndexController', function(){})
.controller('ServicosController', function(){})
.controller('LocalizacaoController', function(){})
.controller('ContatoController', ['$scope', '$http', function($scope, $http){
  $scope.sending = false;
  $scope.contact = {
    name: '',
    mail: '',
    tel: '',
    subject: '',
    msg: ''
  };
  $scope.sendMail = function(){
    $scope.sending = true;
    $http.post('//www.juniauto.com.br/source/envia.php', {
      name: $scope.contact.name,
      mail: $scope.contact.mail,
      tel: $scope.contact.tel,
      subject: $scope.contact.subject,
      msg: $scope.contact.msg
    }).then(function(){
      alert('Mensagem enviada com sucesso.');
     $scope.sending = false;
     location.href = '/';
    }, function(){
      alert('Falha ao enviar a mensagem. Tente novamente ou entre em contato pelos nossos telefones.');
     $scope.sending = false;
    });
  };
}]);